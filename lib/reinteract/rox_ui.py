# Copyright (C) 2008, Thomas Leonard

import os
from rox import saving

class ShellBufferSaveable(saving.Saveable):
    """Save a ShellBuffer using the ROX saving protocol."""

    def __init__(self, buf, notebook):
        self.buf = buf
        self.notebook = notebook

    def save_to_stream(self, stream):
        for chunk_text in self.buf.iterate_text():
            stream.write(chunk_text)
    
    def set_uri(self, uri):
        self.buf.set_filename_and_modified(uri, False)
        self.notebook.set_path([os.path.dirname(os.path.abspath(uri))])

def save_with_rox_savebox(buf, notebook):
    box = saving.SaveBox(ShellBufferSaveable(buf, notebook), buf.filename or 'Worksheet')
    box.show()
